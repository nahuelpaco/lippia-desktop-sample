Feature: As a potential client i need to open a calculator and do math operations

  @Smoke
  Scenario Outline: Resolve operation
    When  Resolve '<num1>' '<operation>' '<num2>'
    Then Result is '<result>'

    @plus
    Examples:
      | num1 | num2 | operation | result |
      | 17   | 8    | plus      | 25     |
    @multiply
    Examples:
      | num1 | num2 | operation | result |
      | 3    | 10   | multiply  | 30     |
    @minus
    Examples:
      | num1 | num2 | operation | result |
      | 390  | 70   | minus     | 320    |
    @divide
    Examples:
      | num1 | num2 | operation | result |
      | 24   | 2    | divide    | 12     |