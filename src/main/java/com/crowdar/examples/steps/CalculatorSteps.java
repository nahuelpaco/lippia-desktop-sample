package com.crowdar.examples.steps;

import com.crowdar.core.PageSteps;
import com.crowdar.examples.services.CalculatorService;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CalculatorSteps extends PageSteps {

    @When("Resolve '(.*)' '(.*)' '(.*)'")
    public void resolve(String num1, String operation, String num2) {
        CalculatorService.resolveOperation(num1, operation, num2);
    }

    @Then("Result is '(.*)'")
    public void result(String result) {
        CalculatorService.result(result);
    }

}