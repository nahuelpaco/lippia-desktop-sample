package com.crowdar.examples.services;

import org.testng.Assert;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.examples.constants.CalculatorConstants;

public class CalculatorService {

    private CalculatorService() {}

    public static void resolveOperation(String num1, String operation, String num2) {

        setDigits(num1);
        ActionManager.click(String.format(CalculatorConstants.OPERATION, operation));
        setDigits(num2);
        ActionManager.click(String.format(CalculatorConstants.EQUAL));
    }

    public static void result(String result) {
        Assert.assertEquals(getResult(), result, "El resultado no es el esperado");
    }

    public static String getResult() {
        return ActionManager.getText(CalculatorConstants.CALCULATOR_RESULTS).replaceAll("\\D+", "").trim();
    }

    public static void setDigits(String num){

        for (int i = 0; i < num.length(); i++){
            ActionManager.click(String.format(CalculatorConstants.NUM, num.charAt(i)));
        }
    }
}
